# Le gourmand

## Installation
```
composer install -o
```

Copy settings files
```
cp .end.example
cp web/sites/default/default.settings.php web/sites/default/settings.php
```

Edit .env and settings php

Install - replace ADMIN_PASS
```
vendor/bin/drush site:install minimal --account-pass=ADMIN_PASS
```



